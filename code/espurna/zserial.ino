/*

SERIAL CONSOLE MODULE

Copyright (C) 2017 by Pablo Pousada <ppousada at com dot uvigo dot es>

*/


#if SERIAL_SUPPORT


const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
 
/*
	SERIAL
*/ 
void serialSetup(){
	DEBUG_MSG_P(PSTR("[SERIAL] Controller Ready\n"));
	Serial.begin(SERIAL_BAUDRATE);
}

void serialLoop(){
	static bool recInProgress=false;
	static byte ndx = 0;
	char startMarker = '<';
	char endMarker = '>';
	char rc;

	if (Serial1.available() > 0) {
		DEBUG_MSG_P(PSTR("[SERIAL] Data in Port Serial1\n"));
	}
    while (Serial.available() > 0) {
		rc = Serial.read();
		if (recInProgress){
		
			if (rc != endMarker) {
				receivedChars[ndx] = rc;
				ndx++;
				if (ndx >= numChars) {
					ndx = numChars - 1;
				}
			} else {
				receivedChars[ndx++] = '\n';
				receivedChars[ndx] = '\r';
				// Inject into Embedis stream
				char* data = &receivedChars[0];
				size_t len = (size_t) ndx;
				settingsInject(data, len);
				ndx = 0;
				recInProgress=false;
			}
		} else if (rc ==startMarker) {
			recInProgress=true;
		}
		
	}
}
#endif